'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('jobs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      job_title: {
        type: Sequelize.STRING
      },
      role: {
        type: Sequelize.STRING
      },
      experience_level: {
        type: Sequelize.STRING
      },
      company_size: {
        type: Sequelize.STRING
      },
      technology: {
        type: Sequelize.STRING
      },
      company_des: {
        type: Sequelize.STRING
      },
      company_size: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('jobs');
  }
};