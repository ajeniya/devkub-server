const nodeMailer = require('nodemailer');

const transporter = nodeMailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'oladimejiajeniya@gmail.com',
        pass: 'ectech1234'
    }
});

module.exports = function signUpEmail(email, username, generateRandomString){

    let mailOptions = {
        from: '"DevKub" <devkub@gmail.com>', // sender address
        to: email, // list of receivers
        subject: 'Welcome to DevKub', // Subject line
        html: `<h1>Welcome ${ username }</h1>
            <p>Thanks you for joining DevKub </p>
            <p>Connect to other dev, build community, ask question write article </p>
            <p>log in to began<p>
            <a href="http://localhost:4200/activate-email?token=${generateRandomString}">
            Click to activate your account </a>
        ` // html body
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
            return res.status(404).send(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
            res.render('index');
    });
};
