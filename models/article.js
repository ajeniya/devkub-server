'use strict';
module.exports = (sequelize, DataTypes) => {
  const article = sequelize.define('article', {
    article_heading: DataTypes.STRING,
    article_body: DataTypes.STRING,
    technology_tag: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    profile_id: DataTypes.INTEGER,
    article_view_count: DataTypes.INTEGER,
    article_wow: DataTypes.INTEGER,                                                         
  }, {
    underscored: true
  });
  article.associate = function(models) {
    // Article belongTo user
    article.belongsTo(models.users);
    article.belongsTo(models.profiles);
    article.hasMany(models.article_comment);
  };
  return article;
};