'use strict';
module.exports = (sequelize, DataTypes) => {
  const article_comment = sequelize.define('article_comment', {
    article_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    profile_id: DataTypes.INTEGER,
    comment: DataTypes.STRING
  }, {underscored: true});
  article_comment.associate = function(models) {
    // article_comment belongTo user
    // article_comment.belongsTo(models.users);
    // article_comment.belongsTo(models.profile);
    article_comment.belongsTo(models.article);
  };
  return article_comment;
};