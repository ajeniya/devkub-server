'use strict';
module.exports = (sequelize, DataTypes) => {
  const role_company = sequelize.define('role_company', {
    user_id: DataTypes.INTEGER,
    profile_id: DataTypes.INTEGER,
    company_name: DataTypes.STRING,
    position: DataTypes.STRING
  }, {
    underscored: true
  });
  role_company.associate = function(models) {
    // associations can be defined here
    role_company.belongsTo(models.profiles);
    role_company.belongsTo(models.users);
  };
  return role_company;
};