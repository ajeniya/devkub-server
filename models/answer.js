'use strict';
module.exports = (sequelize, DataTypes) => {
  const answers = sequelize.define('answer', {
    question_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    profile_id: DataTypes.INTEGER,
    approved_answer: DataTypes.INTEGER,
    upvote: DataTypes.INTEGER,
    downvote: DataTypes.INTEGER,
    flag: DataTypes.INTEGER,
    flag_reason: DataTypes.STRING,
    answer: DataTypes.STRING
  }, {
    underscored: true
  });

  answers.associate = function(models) {
    answers.belongsTo(models.users); 
    answers.belongsTo(models.profiles);
    answers.belongsTo(models.question);
    answers.hasMany(models.answer_comment);
  };
  return answers;
};