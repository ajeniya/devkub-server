'use strict';
module.exports = (sequelize, DataTypes) => {
  const profiles = sequelize.define('profiles', {
    user_id: DataTypes.INTEGER,
    fullname: DataTypes.STRING,
    avater: DataTypes.STRING,
    title: DataTypes.STRING,
    about_me: DataTypes.STRING,
    technology_tag: DataTypes.STRING,
    country: DataTypes.STRING,
    state: DataTypes.STRING,
    city: DataTypes.STRING
  }, {
    underscored: true
  });
  profiles.associate = function(models) {
    // associations can be defined here
    profiles.belongsTo(models.users);
    profiles.hasMany(models.role_company);
  };
  return profiles;
};