'use strict';
module.exports = (sequelize, DataTypes) => {
  const question_comment = sequelize.define('question_comment', {
    question_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    profile_id: DataTypes.INTEGER,
    upvote: DataTypes.INTEGER,
    downvote: DataTypes.INTEGER,
    comment_question: DataTypes.STRING
  }, {
    underscored: true
  });
  question_comment.associate = function(models) {
    // associations can be defined here
    question_comment.belongsTo(models.users); 
    question_comment.belongsTo(models.profiles);
  };
  return question_comment;
};