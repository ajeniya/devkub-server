'use strict';
module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    email: DataTypes.STRING,
    name: DataTypes.STRING,
    country: DataTypes.STRING,
    state: DataTypes.STRING,
    city: DataTypes.STRING,
    password: DataTypes.STRING,
    activation_token: DataTypes.STRING,
    user_activated: DataTypes.INTEGER,
    email_approved: DataTypes.INTEGER,
    email_comment_article: DataTypes.INTEGER,
    email_comment_question: DataTypes.INTEGER,
    developer_approved	: DataTypes.INTEGER,
    article_approved	: DataTypes.INTEGER,
    comment_approved	: DataTypes.INTEGER,
    deleteProfile_approved	: DataTypes.INTEGER,
  }, {
    underscored: true
  });
  users.associate = function(models) {
    // associations can be defined here
    users.hasMany(models.article);
    users.hasMany(models.article_comment);
    users.hasMany(models.jobs);
    users.hasMany(models.question);
    users.hasMany(models.qualification);
    users.hasMany(models.job_apply_later);
    users.hasMany(models.role_company);
    users.hasOne(models.profiles);
  };
  return users;
};