'use strict';
module.exports = (sequelize, DataTypes) => {
  const messages = sequelize.define('message', {
    message_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    message: DataTypes.STRING
  }, {
    underscored: true
  });

  messages.associate = function(models) {
    messages.belongsTo(models.users);
  };
  return messages;
};