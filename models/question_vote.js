'use strict';
module.exports = (sequelize, DataTypes) => {
  const question_vote = sequelize.define('question_vote', {
    question_id: DataTypes.INTEGER,
    upvote: DataTypes.INTEGER,
    downvote: DataTypes.INTEGER
  }, {});
  question_vote.associate = function(models) {
    // associations can be defined here
  };
  return question_vote;
};