'use strict';
module.exports = (sequelize, DataTypes) => {
  const jobs = sequelize.define('jobs', {
    user_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    job_title: DataTypes.STRING,
    role: DataTypes.STRING,
    experience_level: DataTypes.STRING,
    company_size: DataTypes.STRING,
    technology: DataTypes.STRING,
    company_des: DataTypes.STRING,
    company_name: DataTypes.STRING,
    country: DataTypes.STRING,
    state: DataTypes.STRING
  }, {
    underscored: true
  });
  jobs.associate = function(models) {
    // Jobs belongTo user
    jobs.belongsTo(models.users);
    jobs.hasMany(models.job_apply_later);
  };
  return jobs;
};