'use strict';
module.exports = (sequelize, DataTypes) => {
  const question = sequelize.define('question', {
    user_id: DataTypes.INTEGER,
    profile_id: DataTypes.INTEGER,
    question_title: DataTypes.STRING,
    question_body: DataTypes.STRING,
    question_url: DataTypes.STRING,
    question_view: DataTypes.INTEGER,
    upvote: DataTypes.INTEGER,
    downvote: DataTypes.INTEGER,
    approved_answer: DataTypes.INTEGER,
    technology_tag: DataTypes.JSON,
    slug: DataTypes.STRING,
  }, {
    underscored: true
  });
  question.associate = function(models) {
    // question belongTo user
    question.belongsTo(models.users); 
    question.belongsTo(models.profiles);
    question.hasMany(models.answer);
    question.hasMany(models.question_comment);
  };
  return question;
};