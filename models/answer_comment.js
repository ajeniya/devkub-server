'use strict';
module.exports = (sequelize, DataTypes) => {
  const answer_comment = sequelize.define('answer_comment', {
    answer_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    profile_id: DataTypes.INTEGER,
    upvote: DataTypes.INTEGER,
    downvote: DataTypes.INTEGER,
    comment_answer: DataTypes.STRING
  }, {
    underscored: true
  });
  answer_comment.associate = function(models) {
    // associations can be defined here
    answer_comment.belongsTo(models.users); 
    answer_comment.belongsTo(models.profiles);
  };
  return answer_comment;
};