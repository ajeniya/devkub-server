'use strict';
module.exports = (sequelize, DataTypes) => {
  const qualification = sequelize.define('qualification', {
    user_id: DataTypes.INTEGER,
    degree: DataTypes.STRING,
    school: DataTypes.STRING
  }, {});
  qualification.associate = function(models) {
    // qualification belongTo user
    qualification.belongsTo(models.users);
  };
  return qualification;
};