'use strict';
module.exports = (sequelize, DataTypes) => {
  const job_apply_later = sequelize.define('job_apply_later', {
    user_id: DataTypes.INTEGER,
    job_id: DataTypes.INTEGER
  }, {
    underscored: true
  });
  job_apply_later.associate = function(models) {
    // job_apply_later belongTo user
    job_apply_later.belongsTo(models.users);
  };
  return job_apply_later;
};