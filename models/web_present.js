'use strict';
module.exports = (sequelize, DataTypes) => {
  const web_present = sequelize.define('web_present', {
    user_id: DataTypes.INTEGER,
    profile_id: DataTypes.INTEGER,
    twitter: DataTypes.STRING,
    youtube: DataTypes.STRING,
    youtube: DataTypes.STRING,
    linkdin: DataTypes.STRING,
    github: DataTypes.STRING,
    stackoverflow: DataTypes.STRING,
    website: DataTypes.STRING
  }, {
    underscored: true
  });
  web_present.associate = function(models) {
    // associations can be defined here
    web_present.belongsTo(models.users); 
    web_present.belongsTo(models.profiles);
  };
  return web_present;
};