const config = require('config');
const morgan = require('morgan');
const helmet = require('helmet');

const logger = require('./middleware/logger');
const error = require('./middleware/error');
const express = require('express');

const home = require('./routes/home');

const user = require('./routes/user');
const profile = require('./routes/profile');
const Message = require('./models/messages').messages;


const country = require('./routes/country');
const state = require('./routes/state');
const city = require('./routes/city');
const jobApplyLater = require('./routes/job_apply_later');
const job = require('./routes/job');
const qualification = require('./routes/qualification');
const techArticle = require('./routes/tech_article');
const techArticleComment = require('./routes/tech_article_comment');
const question = require('./routes/question');
const answerComment = require('./routes/answer_comment');
const questionComment = require('./routes/question_comment');
const answer = require('./routes/answer');
const webPresent = require('./routes/web_present');
const roleCompany = require('./routes/role_company');

const auth = require('./routes/auth');

// Logger
const winston = require('./config/winston');
const CONSTANTS = require('./config/constants');

//const app = express();

const app = require('express')();
const http = require('http').Server(app);
var server = app.listen(3000);

const io = require('socket.io').listen(server);

io.on('connection', (socket) => {

    /* Get the user's Chat list	*/
    socket.on(`chat-list`, async (data) => {
        if (data.userId == '') {
            this.io.emit(`chat-list-response`, {
                error : true,
                message : CONSTANTS.USER_NOT_FOUND
            });
        }else{
            try {
                const [UserInfoResponse, chatlistResponse] = await Promise.all([
                    queryHandler.getUserInfo( {
                        userId: data.userId,
                        socketId: false
                    }),
                    queryHandler.getChatList( socket.id )
                    ]);

                

                this.io.to(socket.id).emit(`chat-list-response`, {
                    error : false,
                    singleUser : false,
                    chatList : chatlistResponse
                });
                socket.broadcast.emit(`chat-list-response`,{
                    error : false,
                    singleUser : true,
                    chatList : UserInfoResponse
                });
            } catch ( error ) {
                this.io.to(socket.id).emit(`chat-list-response`,{
                    error : true ,
                    chatList : []
                });
            }
        }
    });

    /**
    * send the messages to the user
    */
    socket.on(`add-message`, async (data) => {
        if (data.message === '') {
            this.io.to(socket.id).emit(`add-message-response`,{
                error : true,
                message: CONSTANTS.MESSAGE_NOT_FOUND
            }); 
        }else if(data.fromUserId === ''){
            this.io.to(socket.id).emit(`add-message-response`,{
                error : true,
                message: CONSTANTS.SERVER_ERROR_MESSAGE
            }); 
        }else if(data.toUserId === ''){
            this.io.to(socket.id).emit(`add-message-response`,{
                error : true,
                message: CONSTANTS.SELECT_USER
            }); 
        }else{
            try{
                const [toSocketId, messageResult ] = await Promise.all([
                    queryHandler.getUserInfo({
                        userId: data.toUserId,
                        socketId: true
                    }),
                    queryHandler.insertMessages(data)						
                ]);
                this.io.to(toSocketId).emit(`add-message-response`,data); 
            } catch (error) {
                this.io.to(socket.id).emit(`add-message-response`,{
                    error : true,
                    message : CONSTANTS.MESSAGE_STORE_ERROR
                }); 
            }
        }				
    });


    /**
    * Logout the user
    */
    socket.on('logout', async (data)=>{
        try{
            const userId = data.userId;
            await queryHandler.logout(userId);
            this.io.to(socket.id).emit(`logout-response`,{
                error : false,
                message: CONSTANTS.USER_LOGGED_OUT,
                userId: userId
            });

            socket.broadcast.emit(`chat-list-response`,{
                error : false ,
                userDisconnected : true ,
                userid : userId
            });
        } catch (error) {
            this.io.to(socket.id).emit(`logout-response`,{
                error : true,
                message: CONSTANTS.SERVER_ERROR_MESSAGE,
                userId: userId
            });
        }
    });


    /**
    * sending the disconnected user to all socket users. 
    */
    socket.on('disconnect',async () => {
        socket.broadcast.emit(`chat-list-response`,{
            error : false ,
            userDisconnected : true ,
            userid : socket.request._query['userId']
        });
        
    });

});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, x-auth-token ,X-Requested-With, Content-Type, Accept");
    next();
});


app.use('/', home);
app.use('/api/auth', auth);

app.use('/api/country', country);
app.use('/api/state', state);
app.use('/api/city', city);
app.use('/api/jobapplylater', jobApplyLater)
app.use('/api/job', job);
app.use('/api/qualification', qualification);
app.use('/api/article', techArticle);
app.use('/api/article_comment', techArticleComment);
app.use('/api/web_present', webPresent);
app.use('/api/role_company', roleCompany);

app.use('/api/question', question);
app.use('/api/question_comment', questionComment);
app.use('/api/answer_comment', answerComment);

app.use('/api/answer', answer);

app.use('/api/user', user);
app.use('/api/profile', profile);

app.use('/static', express.static('public'));

app.use(error);

if (!config.get('jwtPrivateKey')){
    console.error('FATAL ERROR: jwtPrivateKey is not defined.');
    process.exit(1);
}

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use(logger);
app.use(helmet());
app.use(morgan('tiny'));

app.use(morgan('combined', { stream: winston.stream }));



//PORT
const port = process.env.PORT || 3000;
app.listen(3000, () => { console.log(`Listening on port ${port}....`)})

