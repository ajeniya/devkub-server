const Sequelize = require('sequelize');
const db = require('../sqlOrm');

const state = db.define('state', {
    name: {
        type: Sequelize.STRING,
        validate: { notEmpty: true }
      },
    country_id: {
        type: Sequelize.INTEGER,
        validate: { notEmpty: true }
      }
},{
  underscored: true
})

module.exports = state;