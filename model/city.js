const Sequelize = require('sequelize');
const db = require('../sqlOrm');

const city = db.define('city', {
    name: {
        type: Sequelize.STRING,
        validate: { notEmpty: true }
      },
    state_id: {
        type: Sequelize.INTEGER,
        validate: { notEmpty: true }
      }    
},{
  underscored: true
})

module.exports = city;