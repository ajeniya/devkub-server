const Sequelize = require('sequelize');
const db = require('../sqlOrm');

const country = db.define('country', {
    sortname: {
        type: Sequelize.STRING,
        validate: { notEmpty: true }
      },
    name: {
        type: Sequelize.STRING,
        validate: { notEmpty: true }
      },
    phonecode: {
        type: Sequelize.STRING,
        validate: { notEmpty: true }
    }
},{
    underscored: true
})

module.exports = country;