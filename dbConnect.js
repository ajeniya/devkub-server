const mysql = require('mysql');
const nodeMailer = require('nodemailer');
const winston = require('./config/winston');

const connection = mysql.createConnection({
    port: '8889',
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'node'
  }); 
 
  connection.connect((err) => {
    if (err){
        console.log(err);
        console.log('Not connected!');
        let transporter = nodeMailer.createTransport({
          host: 'smtp.gmail.com',
          port: 465,
          secure: true,
          auth: {
              user: 'oladimejiajeniya@gmail.com',
              pass: 'ectech1234'
          }
      });
      let mailOptions = {
          from: '"Web server database connection error" <oladimejiajeniya@gmail.com>', // sender address
          to: 'oladimejiajeniya@gmail.com', // list of receivers
          subject: 'Database connection error', // Subject line
          text: 'Web server database connection error', // plain text body
          html: '<b>Web server database connection error</b>' // html body
      };
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
            res.render('index');
        });

        winston.error(`${err.status || 500} - ${err.message} --- ${'Application failed to connect to database'}`);


    }else{
      console.log('Connected!');
    };

    
  });


module.exports = connection;