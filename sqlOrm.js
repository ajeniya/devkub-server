const Sequelize = require('sequelize');
const nodeMailer = require('nodemailer');
const winston = require('./config/winston');

const sequelize = new Sequelize('devcube', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',
    port: 8889,
    operatorsAliases: false,
  
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
  });

  sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.log('Not connected!');
    let transporter = nodeMailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
          user: 'oladimejiajeniya@gmail.com',
          pass: 'ectech1234'
      }
  });
  let mailOptions = {
      from: '"Web server database connection error" <oladimejiajeniya@gmail.com>', // sender address
      to: 'oladimejiajeniya@gmail.com', // list of receivers
      subject: 'Database connection error from devcube server ORM', // Subject line
      text: 'Web server database connection error', // plain text body
      html: '<b>Web server database connection error</b>' // html body
  };
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
        return console.log(error);
    }
    console.log('Message %s sent: %s', info.messageId, info.response);
        res.render('index');
    });

    winston.error(`${err.status || 500} - ${err.message} --- ${'Application failed to connect to database'}`);

  });

  
  module.exports = sequelize;

