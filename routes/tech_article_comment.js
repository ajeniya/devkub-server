const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');

const ArticleComment = require('../models').article_comment;

const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/', (req, res, next) => {
    try {
        ArticleComment.findAndCountAll().then(JobResult => {
            res.status(200).send(
                {
                    count: JobResult.count,
                    rows:  JobResult.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/single', (req, res, next) => {
    try {
        const articleId = req.body.tech_article_id;

        ArticleComment.findAll({
            where: {tech_article_id: articleId }
          }).then(result => {
            
            if(_.isEmpty(result)){
                return res.status(404).send('The article comment with the given ID was not found');
            }
            
            res.status(200).send(result);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.post('/', async(req, res, next) => {
    try{
        ArticleComment.create({ 
            user_id: req.body.user_id,
            tech_article_id: req.body.tech_article_id,
            comment: req.body.comment
         }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    } catch (error){
        next(error); 
    }
})


router.put('/', (req, res) => {
    try {
        const articleCommentId = parseInt(req.body.id);
        const comment =  req.body.comment;

        ArticleComment.update({
                comment: comment
            },{ // Clause
                where: {
                    id: articleCommentId
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request article comment does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.body.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.delete('/:id', [auth, admin], (req, res, next) => {
    try {
        const articleCommentId = parseInt(req.params.id);
        ArticleComment.destroy({
            where: {
                id: articleCommentId
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('Record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;