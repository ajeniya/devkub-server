const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');
const State = require('../model/state');

const _ = require("lodash");
const bodyParser = require('body-parser');
const multer  = require('multer');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/', (req, res, next) => {
    try {
        State.findAll().then(StateResult => {
            res.status(200).send(StateResult);
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.post('/single', (req, res, next) => {
    try {
        const countryId = parseInt(req.body.country_id);
        State.findAll({
            where: {country_id: countryId }
          }).then(StateResult => {
            
            if(_.isEmpty(StateResult)){
                return res.status(404).send('The State with the given STATE_ID was not found');
            }
            
            res.status(200).send(StateResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});


router.post('/', async(req, res, next) => {
    try {    
        State.findOrCreate({where: {country_id: req.body.country_id}, 
            defaults: { country_id: req.body.country_id, 
                        name: req.body.name
                 }}).spread((user, created) => {
                
                if(created == false){
                    return res.status(404).send('User profile already exist');
                }

                const result = {
                        country_id: req.body.country_id,
                        name:  req.body.name
                }

                res.status(200).send(result);
            
            });

    } catch (error) {
        next(error);
    }
});


router.put('/', (req, res, next) => {
    try {
        const countryId =  req.body.country_id;
        const name = req.body.name;

        State.update(
            {
                name: name
            },
            { // Clause
                where: 
                {
                    country_id: countryId
                }
            }
        ).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('The request account does not exist'); 
            }
            const updatedDataFromTheDb = {
                country_id: countryId,
                name: name
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            res.status(403).send('Email taken by different user');
        });

    } catch (error) {
        next(error);
    }
});

router.delete('/:country_id', [auth, admin], (req, res, next) => {
    try {
        State.destroy({
            where: {
                country_id: req.params.country_id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;