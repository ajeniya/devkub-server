const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');
const Country = require('../model/country');

const _ = require("lodash");
const bodyParser = require('body-parser');
const multer  = require('multer');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/', (req, res, next) => {
    try {
        Country.findAll().then(cityResult => {
            res.status(200).send(cityResult);
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/single', (req, res, next) => {
    try {
        const country_id = parseInt(req.body.country_id);
        Country.findAll({
            where: {id: country_id }
          }).then(countryResult => {
            
            if(_.isEmpty(countryResult)){
                return res.status(404).send('The country with the given country was not found');
            }
            
            res.status(200).send(countryResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});


router.post('/', async(req, res, next) => {
    try {    
        Country.findOrCreate({where: {id: req.body.id}, 
            defaults: { 
                    id: req.body.country_id, 
                    sortname: req.body.sortname,
                    name: req.body.name,
                    phonecode: req.body.phonecode,
                 }}).spread((user, created) => {
                
                if(created == false){
                    return res.status(404).send('Country already exist');
                }

                const result = {
                    id: req.body.country_id, 
                    sortname: req.body.sortname,
                    name: req.body.name,
                    phonecode: req.body.phonecode
                }

                res.status(200).send(result);
            
            });

    } catch (error) {
        next(error);
    }
});


router.put('/', (req, res, next) => {
    try {
        const countryId =  req.body.country_id;
        const name = req.body.name;
        const sortname = req.body.sortname;
        const phonecode = req.body.phonecode;

        Country.update(
            // Values to update
            {
                name: name,
                sortnam: sortname,
                phonecode: phonecode
            },
            { // Clause
                where: 
                {
                    id: countryId
                }
            }
        ).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('The request country does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: countryId,
                name: name,
                sortnam: sortname,
                phonecode: phonecode
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            res.status(403).send('Incomplete fields');
        });

    } catch (error) {
        next(error);
    }
});

router.delete('/:country_id', (req, res, next) => {
    try {
        Country.destroy({
            where: {
                id: req.params.country_id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;