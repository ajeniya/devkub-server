const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.status(200).send({ title: 'My Express App', message: 'Hello'});
});

module.exports = router;