const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');
const Joi = require('joi');

const user = require('../models').users;
const profile = require('../models').profiles;
const answer_comment = require('../models').answer_comment;

const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();
router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 

router.get('/user', async(req, res, next) => {
    try {
        const QuestionCommentId = parseInt(req.body.user_id);

        answer_comment.findAndCountAll({
            include: [user],
            where: {id: QuestionCommentId }
          }).then(questionsAnswerResult => {
            
            if(_.isEmpty(questionsAnswerResult)){
                return res.status(404).send('The question with the given ID was not found');
            }
            
            res.status(200).send( {
                count: questionsAnswerResult.count,
                rows:  questionsAnswerResult.rows
            });

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.get('/', async(req, res, next) => {
    try {
        answer_comment.findAndCountAll(
            {include: [user,profile]}
        ).then(QuestionAnswerResult => {
            res.status(200).send(
                {
                    count: QuestionAnswerResult.count,
                    rows:  QuestionAnswerResult.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/:id', async(req, res, next) => {
    try {
        const Id = parseInt(req.params.id);

        answer_comment.findAll({
            include: [user,profile],
            where: {id: Id }
          }).then(answerCommentResult => {
            
            if(_.isEmpty(answerCommentResult)){
                return res.status(404).send('The question with the given ID was not found');
            }
            
            res.status(200).send(answerCommentResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});


router.post('/', async(req, res, next) => {
    try{
        const {user_id, answer_id, profile_id, comment_answer} = req.body;

        const errorResult = validateAnswerComment(req.body);
        if (errorResult.error) {
            res.status(400).send(errorResult.error.details[0].message);
            return;
        }

        answer_comment.create({ 
            user_id: user_id,
            answer_id: answer_id,
            profile_id: profile_id,
            comment_answer: comment_answer
         }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    } catch (error){
        next(error); 
    }
});

router.post('/upvote', async(req, res, next) => {
    try{

        const answerCommentId = req.body.answerCommentId;

        answer_comment.findByPk(answerCommentId).then(answerResult => {
            return answerResult.increment('upvote', {by: 1})
        }).catch((error) => {
            console.log(error)
        });

        res.status(200).send(true);

    }catch(error){
        next(error);
    }
});

router.post('/downvote', async(req, res, next) => {
    try{
        const answerCommentId = req.body.answerCommentId;

        answer_comment.findByPk(answerCommentId).then(article => {
            return article.increment('downvote', {by: 1})
        }).catch((error) => {
            console.log(error)
        });

        res.status(200).send(true);

    }catch(error){
        next(error);
    }
});

router.put('/:id', async(req, res) => {
    try {
        const Id = parseInt(req.params.id);

        const comment_answer =  req.body.comment_answer;

        answer_comment.update({
                comment_answer: comment_answer,
            },{ // Clause 
                where: {
                    id: Id
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The comment answer does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});


router.delete('/:id', [auth, admin],  async(req, res, next) => {
    try {
        const Id = parseInt(req.params.id);
        answer_comment.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

function validateAnswerComment(data) {
    const schema = {
        user_id: Joi.number().required(),
        answer_id: Joi.number().required(),
        profile_id: Joi.number().required(),
        comment_answer: Joi.string().required()
    };

    return Joi.validate(data, schema); 
}

module.exports = router;