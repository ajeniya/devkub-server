const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');

const Question = require('../models').question;
const profile = require('../models').profiles;
const user = require('../models').users;

const answer = require('../models').answer;
const question_comment = require('../models').question_comment;


const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 

router.get('/user', (req, res, next) => {
    try {
        const userQuestionId = parseInt(req.body.user_id);

        Question.findAndCountAll({
            where: {user_id: userQuestionId }
          }).then(userQuestionsResult => {
            
            if(_.isEmpty(userQuestionsResult)){
                return res.status(404).send('The question with the given ID was not found');
            }
            
            res.status(200).send( {
                count: userQuestionsResult.count,
                rows:  userQuestionsResult.rows
            });

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.get('/', (req, res, next) => {
    try {
        Question.findAndCountAll({
            order: [
                // will return `username` DESC
                ['created_at', 'DESC'],
              ],
            include: [user,profile,answer],
        }).then(QuestionResult => {
            res.status(200).send(
                {
                    count: QuestionResult.count,
                    rows:  QuestionResult.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/:id', (req, res, next) => {
    try {
        const QuestionId = parseInt(req.params.id);

        Question.findOne({
            include: [user, profile, answer, question_comment],
            where: {id: QuestionId }
          }).then(jobResult => {
            
            if(_.isEmpty(jobResult)){
                return res.status(404).send('The question with the given ID was not found');
            }

            Question.findByPk(QuestionId).then(article => {
                return article.increment('question_view', {by: 1})
            });
            
            res.status(200).send( jobResult );

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

/**
 * Remove non-word chars.
 */
function removeNonWord(str){
    return str.replace(/[^0-9a-zA-Z\xC0-\xFF \-]/g, '');
  }

/**
 * Convert to lower case, remove accents, remove non-word chars and
 * replace spaces with the specified delimeter.
 * Does not split camelCase text.
 */
function slugify(str, delimeter){
    if (delimeter == null) {
        delimeter = "-";
    }
  
    str = removeNonWord(str);
    str = trim(str) //should come after removeNonWord
            .replace(/ +/g, delimeter) //replace spaces with delimeter
            .toLowerCase();
  
    return str;
  }



router.post('/',  auth, async(req, res, next) => {
    try{

        const url = req.body.titleBody;
        // console.log(url);
        Question.create({ 
            user_id: req.user.user_id,
            profile_id: req.user.profile_id,
            question_title: req.body.titleBody,
            question_body: req.body.question,
            question_url: url.replace(/\s+/g, '-').toLowerCase(),
            slug: req.body.titleBody.replace(/[^a-zA-Z\s]/g,"").replace(/[\s]/g,"-").toLowerCase().substring(0, 150), //new object assigned to var str
            technology_tag: req.body.tags,
         }).then((created)=> {
            console.log(created)
            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    } catch (error){
        next(error); 
    }
});



router.post('/upvote', (req, res) => {
    try {
        const questionId =  req.body.id;
        Question.findByPk(questionId).then(question => {
            return question.increment('upvote', {by: 1})
          }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

        } catch (error) {
            console.log(error);
        }
});

router.post('/downvote', (req, res) => {
    try {
        const questionId =  req.body.id;

        Question.findByPk(questionId).then(user => {
            return user.increment('downvote', {by: 1})
          }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

        } catch (error) {
            console.log(error);
        }
});

router.put('/approved_answer', (req, res) => {
    try {
        const user_id =  req.body.user_id;
        const questionId = req.body.question_id;

        Question.update({
            approved_answer: 1,
            },{ // Clause 
                where: {
                    id: questionId
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request question does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.put('/:id', (req, res) => {
    try {
        const questionId = parseInt(req.params.id);

        const question_title =  req.body.question_title;
        const question_body = req.body.question_body;
        const technology_tag = req.body.technology_tag;

        Question.update({
                question_title: question_title,
                question_body: question_body,
                technology_tag: technology_tag,
            },{ // Clause 
                where: {
                    id: questionId
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request question does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});


router.delete('/:id', [auth, admin],  (req, res, next) => {
    try {
        const Id = parseInt(req.params.id);
        Question.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;