const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');

const jobApplyLater = require('../models').job_apply_later;
const user = require('../models').users;

const _ = require("lodash");
const bodyParser = require('body-parser');
const multer  = require('multer');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/', (req, res, next) => {
    try {
        jobApplyLater.findAll(
            { include: [user] }
        ).then(jobApplyLaterResult => {
            res.status(200).send(jobApplyLaterResult);
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/jobApplyLater', (req, res, next) => {
    try {
        const id = parseInt(req.body.id);
        jobApplyLater.findOne({
            where: {id: id }
          }).then(jobApplyLaterResult => {
            
            if(_.isEmpty(jobApplyLaterResult)){
                return res.status(404).send('The jobApplyLater with the given APPLY was not found');
            }
            
            res.status(200).send(jobApplyLaterResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});


router.post('/', async(req, res, next) => {
    try {    
        jobApplyLater.findOrCreate({where: {job_id: req.body.job_id,user_id: req.body.user_id}, 
            defaults: { job_id: req.body.job_id, 
                        user_id: req.body.user_id
                 }}).spread((user, created) => {
                
                if(created == false){
                    return res.status(404).send('Job already exist');
                }

                const result = {
                    job_id: req.body.job_id,
                    user_id:  req.body.user_id
                }

                res.status(200).send(result);
            
            });

    } catch (error) {
        next(error);
    }
});


router.put('/', (req, res, next) => {
    try {
        const jobId =  req.body.job_id;
        const userId = req.body.user_id;
        const Id = req.body.id;

        jobApplyLater.update(
            // Values to update
            {
                job_id: jobId,
                user_id: userId
            },
            { // Clause
                where: 
                {
                    id: Id
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request job apply later does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: Id,
                job_id: jobId,
                user_id: userId
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            res.status(403).send('Email taken by different user');
        });

    } catch (error) {
        next(error);
    }
});

router.delete('/delete', [auth, admin], (req, res, next) => {
    try {
        const Id =  req.body.id;

        jobApplyLater.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;