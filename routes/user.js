const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');
const winston = require('../config/winston');

const User = require('../models').users;
const article = require('../models').article;
const job = require('../models').jobs;
const profiles = require('../models').profiles;

///////////////////////////////////////////
///// Email 
///////////////////////////////////////////
const signUpEmail = require('../emails/welcome_email');

const _ = require("lodash");
const Joi = require('joi');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const multer  = require('multer');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/me', auth, (req, res, next) => {
    try {
        User.findOne({
            where: {id: req.user.user_id },
            attributes: { exclude: ['password','activation_token'] },
            include: [profiles]
          }).then(account => {

            res.status(200).send(account);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
          
    } catch (error) {
        next(error);
    }
    
});

router.get('/', (req, res, next) => {
    try {
        User.findAll({
            include: [profiles],
            attributes: { exclude: ['password'] }
        }).then(userObject => {
            res.status(200).send(userObject);
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        
        next(error);
    }   
});

router.get('/:id', (req, res, next) => {
    try {
        const userId = parseInt(req.params.id);
        User.findOne({
            where: {id: userId },
             attributes: { exclude: ['password'] }
          }).then(user => {
            
            if(_.isEmpty(user)){
                return res.status(404).send('The user with the given ID was not found');
            }
            
            res.status(200).send(user);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});



router.post('/messagies_privacy', auth, (req, res, next) => {
    try{
        const developerPrivacySetting = req.body.developer;
        const articlePrivacySetting = req.body.article;
        const commentPrivacySetting = req.body.comment;

        User.update(
            {
                developer_approved: developerPrivacySetting,
                article_approved: articlePrivacySetting,
                comment_approved: commentPrivacySetting
            },
            { // Clause
                where: 
                {
                    id: res.user.user_id
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('Unable to update messages privacy setting, Try again'); 
            }
            
            res.status(200).send(true);

        }).catch((error)=> {
            res.status(403).send(error);
        });

    } catch{
        next(error);
    }
    
})

router.post('/delete_privacy', auth, (req, res, next) => {
    try{
        User.destroy(
            { // Clause
                where: 
                {
                    id: res.user.id
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('Unable to delete profile, Try again'); 
            }
            
            res.status(200).send(true);

        }).catch((error)=> {
            res.status(403).send(error);
        });

    } catch{
        next(error);
    }
    
});


router.post('/', async(req, res, next) => {
    try {
        const result = validateAccount(req.body);
        if (result.error) {
            res.status(400).send(result.error.details[0].message);
            return;
        }

        User.findOne({
            where: {
                email: req.body.email,
                name: req.body.name
            }
          }).then(data => {
              console.log(data);

            if(data.dataValues.name == req.body.name) {
                console.log('Username exist in db')
              return res.status(404).send('Username exist');
            }

          });

        bcrypt.hash(req.body.password, 10, function(err, hashPassword) {

            let generateRandomString = randStr(40)
    
            User.findOrCreate({where: {email: req.body.email, name: req.body.name}, 
                defaults: {
                    name: req.body.name, 
                    password: hashPassword,
                    activation_token: generateRandomString,
                 }}).spread((user, created) => {

                
                if(created == false){
                    return res.status(404).send('Email exist');
                }


                signUpEmail(req.body.email, req.body.name, generateRandomString);


                const result = {
                        id: user.dataValues.id,
                        name:  req.body.name,
                        email: req.body.email
                }

                const token = jwt.sign({
                    "user_id": user.dataValues.id,
                    "name": req.body.name,
                    "email": req.body.email,
                }, config.get('jwtPrivateKey'));

                profiles.findOrCreate({where: {user_id: user.dataValues.id},
                     defaults: {user_id: user.dataValues.id}})
                    .spread((user, created) => {
                        console.log(user.get({
                        plain: true
                        }))
                        console.log(created)
                    });

                res.header('token', token).status(200).send([token, result]);
            
            });
         }); 

    } catch (error) {
        winston.error(`${error.status || 500} - ${error.message}`);
        next(error);
    }
});

router.post('/emailprivacy', [auth], (req, res, next) => {
    try{
        const activationToken = (req.body.email == true) ? 1 : 0;
        const emailCommentArticle = (req.body.email_comment_article == true) ? 1 : 0;
        const emailCommentQuestion = (req.body.email_comment_question == true) ? 1 : 0;

        User.update(
            // Values to update
            {
                email_approved: activationToken,
                email_comment_article: emailCommentArticle,
                email_comment_question: emailCommentQuestion,
            },
            { // Clause
                where: 
                {
                    id: req.user.user_id
                }
            }
        ).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('Unable to update email privacy setting, Try again'); 
            }
            
            res.status(200).send(true);

        }).catch((error)=> {
            res.status(403).send('Unable to update email privacy setting,');
        });

    } catch{
        next(error);
    }
    
});

router.post('/privacymessage', auth, async(req, res, next) => {
    try{

        const developerPrivacySetting = (req.body.developer == true) ? 1 : 0;
        const articlePrivacySetting = (req.body.article == true) ? 1 : 0;
        const commentPrivacySetting = (req.body.comment == true) ? 1 : 0;

        User.update(
            // Values to update
            {
                developer_approved: developerPrivacySetting,
                article_approved: articlePrivacySetting,
                comment_approved: commentPrivacySetting
            },
            { // Clause
                where: 
                {
                    id: req.user.user_id
                }
            }
        ).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('Unable to update messages privacy setting, Try again'); 
            }
            
            res.status(200).send(true);

        }).catch((error)=> {
            res.status(403).send('Unable to update email privacy setting,');
        });

    } catch{
        next(error);
    }
    
});

router.post('/activate', (req, res, next) => {
    try{
        const activationToken = req.body.token;

        User.update(
            // Values to update
            {
                user_activated: 1
            },
            { // Clause
                where: 
                {
                    activation_token: activationToken
                }
            }
        ).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('Unable to activate account, Try again'); 
            }
            
            res.status(200).send(true);

        }).catch((error)=> {
            res.status(403).send('Unable to activate account');
        });

    } catch{
        next(error);
    }
    
});


router.post('/location', auth, async(req, res, next) => {
    try{
        const country = req.body.country;
        const state = req.body.state;
        const city = req.body.city;

        User.update(
            // Values to update
            {
                country: country,
                state: state,
                city: city
            },
            { // Clause
                where: 
                {
                    id: req.user.user_id
                }
            }
        ).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('Unable to activate account, Try again'); 
            }
            
            res.status(200).send(true);

        }).catch((error)=> {
            res.status(403).send('Unable to activate account');
        });

    } catch{
        next(error);
    }
    
});


router.put('/:id', auth, (req, res, next) => {
    try {
        const userId = parseInt(req.params.id);
        const result = validateAccountUpdate(req.body);
        if (result.error) {
            res.status(400).send(result.error.details[0].message);
            return;
        }

        const name =  req.body.name;
        const email = req.body.email;

        User.update(
            // Values to update
            {
                name:  name,
                email: email
            },
            { // Clause
                where: 
                {
                    id: userId
                }
            }
        ).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('The request account does not exist'); 
            }
            const updatedDataFromTheDb = {
                 id: req.params.id,
                 name:  req.body.name,
                 email: req.body.email
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            res.status(403).send('Email taken by different user');
        });

    } catch (error) {
        next(error);
    }
});

router.post('/delete', [auth], (req, res, next) => {
    try {
        User.destroy({
            where: {
                id: req.user.user_id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

function validateAccount(account) {
    const schema = {
        email: Joi.string().required().email({ minDomainAtoms: 2 }),
        name: Joi.string().min(3).max(50).required(),
        password: Joi.string().required().min(5).max(50)

    };

    return Joi.validate(account, schema); 
}

function validateAccountUpdate(account) {
    const schema = {
        email: Joi.string().min(5).max(200).email().required(),
        name: Joi.string().min(5).max(50).required(),
    };

    return Joi.validate(account, schema); 
}

function randStr(len, chars='aggydhjehufhrbc123456789') {
    let s = '';
    while (len--) s += chars[Math.floor(Math.random() * chars.length)];
    return s;
  }


module.exports = router; 