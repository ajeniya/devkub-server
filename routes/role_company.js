const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');

const profile = require('../models').profiles;
const user = require('../models').users;
const roleCompany = require('../models').role_company;


const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/', (req, res, next) => {
    try {
        roleCompany.findAndCountAll(
            { include: [user, profile] }
        ).then(roleCompanyResult => {
            res.status(200).send(
                {
                    count: roleCompanyResult.count,
                    rows:  roleCompanyResult.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/:id', (req, res, next) => {
    try {
        const roleCompanyId = parseInt(req.params.id);

        roleCompany.findAll({
            where: {id: roleCompanyId }
          }).then(roleCompanyResult => {
            
            if(_.isEmpty(roleCompanyResult)){
                return res.status(404).send('The roleCompany with the given ID was not found');
            }
            
            res.status(200).send(roleCompanyResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.post('/', async(req, res, next) => {
    try{
        roleCompany.create({ 
            user_id: req.body.user_id,
            profile_id: req.body.profile_id,
            company_name: req.body.company_name,
            position: req.body.position
         }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    } catch (error){
        next(error); 
    }
})


router.put('/:id', (req, res) => {
    try {
        const roleCompanyId = parseInt(req.params.id);

        const company_name =  req.body.company_name;
        const position =  req.body.position;

        roleCompany.update({
                company_name: company_name,
                position: position
            },{ // Clause
                where: {
                    id: roleCompanyId
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request job does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.delete('/:id', [auth, admin],  (req, res, next) => {
    try {
        const Id = parseInt(req.params.id);
        roleCompanyId.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;