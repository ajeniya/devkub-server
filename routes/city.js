const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');
const City = require('../model/city');

const _ = require("lodash");
const bodyParser = require('body-parser');
const multer  = require('multer');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/', (req, res, next) => {
    try {
        City.findAll().then(cityResult => {
            res.status(200).send(cityResult);
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.post('/cities', (req, res, next) => {
    try {
        const state_id = parseInt(req.body.state_id);
        City.findAll({
            where: {state_id: state_id }
          }).then(cityResult => {
            
            if(_.isEmpty(cityResult)){
                return res.status(404).send('The city with the given STATE_ID was not found');
            }
            
            res.status(200).send(cityResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});


router.post('/', async(req, res, next) => {
    try {    
        City.findOrCreate({where: {state_id: req.body.state_id}, 
            defaults: { state_id: req.body.state_id, 
                        name: req.body.name
                 }}).spread((user, created) => {
                
                if(created == false){
                    return res.status(404).send('User profile already exist');
                }

                const result = {
                        state_id: req.body.state_id,
                        name:  req.body.name
                }

                res.status(200).send(result);
            
            });

    } catch (error) {
        next(error);
    }
});


router.put('/', (req, res, next) => {
    try {
        const stateId =  req.body.state_id;
        const name = req.body.name;

        City.update(
            // Values to update
            {
                name: name
            },
            { // Clause
                where: 
                {
                    state_id: stateId
                }
            }
        ).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('The request account does not exist'); 
            }
            const updatedDataFromTheDb = {
                state_id: stateId,
                name: name
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            res.status(403).send('Email taken by different user');
        });

    } catch (error) {
        next(error);
    }
});

router.delete('/:state_id', [auth, admin], (req, res, next) => {
    try {
        City.destroy({
            where: {
                state_id: req.params.state_id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;