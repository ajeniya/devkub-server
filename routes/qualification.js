const express = require('express');

const Qualification = require('../models').qualification;

const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/', (req, res, next) => {
    try {
        Qualification.findAndCountAll().then(Result => {
            res.status(200).send(
                {
                    count: Result.count,
                    rows:  Result.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/:id', (req, res, next) => {
    try {
        const Id = parseInt(req.params.id);

        Qualification.findAll({
            where: {id: Id }
          }).then(qualificationResult => {
            
            if(_.isEmpty(qualificationResult)){
                return res.status(404).send('The job with the given QUALIFICATION_ID was not found');
            }
            
            res.status(200).send(qualificationResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.post('/', async(req, res, next) => {
    try{
        Qualification.create({ 
            user_id: req.body.user_id,
            degree: req.body.degree,
            school: req.body.school
         }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    } catch (error){
        next(error); 
    }
})


router.put('/', (req, res) => {
    try {

        const Id = req.body.id;
        const user_id = req.body.user_id;
        const degree =  req.body.degree;
        const school = req.body.school;

        Qualification.update({
                user_id: user_id,
                degree: degree,
                school: school,
            },{ // Clause
                where: {
                    id: Id
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request qualification does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.body.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.delete('/:id',  (req, res, next) => {
    try {
        const Id = parseInt(req.params.id);
        Qualification.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;