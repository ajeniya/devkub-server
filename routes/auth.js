const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');
const db = require('../dbConnect');
const _ = require("lodash");
const Joi = require('joi');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');

const User = require('../models').users;
const Profile = require('../models').profiles;

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 

router.post('/login', (req, res) => {

    const result = validateAccount(req.body);
    
    if (result.error) {
        res.status(400).send(result.error.details[0].message);
        return;
    }

    const password = req.body.password;

    User.findOne({
            include: [Profile],
            where: {email: req.body.email }
          }).then(account => {

        if(_.isEmpty(account)){
            return res.status(404).send('Invalid email');
        }

        bcrypt.compare( password, account.password, function(passErr, PassResponse) {
            if(PassResponse) {
                if(!_.isNull(account.profile)){
                    const token = jwt.sign({
                        "user_id": account.id,
                        "username": account.name,
                        "email": account.email,
                        "profile_id": account.profile.id,
                        "fullname": account.profile.fullname,
                        "avater": account.profile.avater,
                        "title": account.profile.title,
                        "technonoly_tag": account.profile.technonoly_tag,
                        "country": account.profile.country,
                        "state": account.profile.state,
                        "city": account.profile.city,
                    }, config.get('jwtPrivateKey'));

                    const tokenResponse = { 'token': token};
                    res.header('x-auth-token', token).status(200).send(tokenResponse);
                }
                const token = jwt.sign({
                    "user_id": account.id,
                    "username": account.name,
                    "email": account.email
                }, config.get('jwtPrivateKey'));

                //res.status(200).send(token);
                const tokenResponse = { 'token': token};
                res.header('x-auth-token', token).status(200).send(tokenResponse);

            } else {
                // Passwords don't match
                res.send("Incorrect password");
            } 
        });

        }).catch( (error)=> {
            res.status(400).send(error);
        })
});


function validateAccount(account) {
    const schema = {
        email: Joi.string().min(5).max(200).email().required(),
        password: Joi.string().min(5).max(50).required(),
    };

    return Joi.validate(account, schema); 
}

module.exports = router;