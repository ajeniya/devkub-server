const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');

const profile = require('../models').profiles;
const user = require('../models').users;
const webPresent = require('../models').web_present;


const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/', (req, res, next) => {
    try {
        webPresent.findAndCountAll(
            { include: [user, profile] }
        ).then(webPresentResult => {
            res.status(200).send(
                {
                    count: webPresentResult.count,
                    rows:  webPresentResult.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/:id', (req, res, next) => {
    try {
        const webPresentId = parseInt(req.params.id);

        webPresent.findAll({
            where: {id: webPresentId }
          }).then(webPresentResult => {
            
            if(_.isEmpty(webPresentResult)){
                return res.status(404).send('The webPresent with the given ID was not found');
            }
            
            res.status(200).send(webPresentResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.post('/', async(req, res, next) => {
    try{
        webPresent.create({ 
            user_id: req.body.user_id,
            profile_id: req.body.profile_id,
            twitter	: req.body.twitter	,
            youtube: req.body.youtube,
            linkdin: req.body.linkdin,
            github: req.body.	github,
            stackoverflow: req.body.stackoverflow,
            website: req.body.website
         }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    } catch (error){
        next(error); 
    }
})


router.put('/:id', (req, res) => {
    try {
        const webpresentId = parseInt(req.params.id);

        const user_id =  req.body.user_id;
        const profile_id =  req.body.profile_id;
        const twitter = req.body.twitter;
        const youtube = req.body.youtube;
        const linkdin = req.body.linkdin;
        const github = req.body.github;
        const stackoverflow = req.body.stackoverflow;
        const website = req.body.website;

        webPresent.update({
                user_id: user_id,
                profile_id: profile_id,
                twitter: twitter,
                youtube: youtube,
                linkdin: linkdin,
                github: github,
                stackoverflow: stackoverflow,
                website: website
            },{ // Clause
                where: {
                    id: webpresentId
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request job does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.delete('/:id', [auth, admin],  (req, res, next) => {
    try {
        const Id = parseInt(req.params.id);
        webPresent.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;