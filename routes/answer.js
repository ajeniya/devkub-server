const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');
const Joi = require('joi');

const answer = require('../models').answer;
const profile = require('../models').profiles;
const user = require('../models').users;
const answer_comment = require('../models').answer_comment;


const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 

router.get('/user', auth, async(req, res, next) => {
    try {
        const AnswerId = parseInt(req.body.answer_id);

        answer.findAndCountAll({
            where: {id: AnswerId }
          }).then(answerResult => {
            
            if(_.isEmpty(answerResult)){
                return res.status(404).send('The question with the given ID was not found');
            }
            
            res.status(200).send( {
                count: answerResult.count,
                rows:  answerResult.rows
            });

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.get('/', auth, async(req, res, next) => {
    try {
        answer.findAndCountAll(
             {include: [user,profile]}
        ).then(answerResult => {
            res.status(200).send(
                {
                    count: answerResult.count,
                    rows:  answerResult.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});

router.get('/:id', auth, async(req, res, next) => {
    try {
        const AnswerId = req.params.id;

        answer.findAndCountAll({
            include: [user,profile],
            where: {question_id: AnswerId }
          }).then(answerResult => {
            
            if(_.isEmpty(answerResult)){
                return res.status(404).send('The answer with the given ID was not found');
            }
            
            res.status(200).send(
                {
                    count: answerResult.count,
                    rows:  answerResult.rows
                }
            );

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.post('/', auth, async(req, res, next) => {
    try{
        answer.create({ 
            user_id: req.user.user_id,
            profile_id: req.user.profile_id,
            question_id: req.body.question_id,
            answer: req.body.answer
         }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    } catch (error){
        next(error); 
    }
});

router.post('/upvote', auth, async(req, res, next) => {
    try{

        const answerId = parseInt(req.body.answerId);

        answer.findByPk(answerId).then(answerResult => {
            return answerResult.increment('upvote', {by: 1})
        }).catch((error) => {
            console.log(error)
        });

        res.status(200).send(true);

    }catch(error){
        next(error);
    }
});

router.post('/downvote', auth, async(req, res, next) => {
    try{
        const answerId = parseInt(req.body.answerId);

        answer.findByPk(answerId).then(article => {
            return article.increment('downvote', {by: 1})
        }).catch((error) => {
            console.log(error)
        });

        res.status(200).send(true);

    }catch(error){
        next(error);
    }
});

router.post('/flag', auth, async(req, res, next) => {
    try{
        const answerId = parseInt(req.body.answerId);

        answer.findByPk(answerId).then(article => {
            return article.increment('flag', {by: 1})
        }).catch((error) => {
            console.log(error)
        });

        res.status(200).send(true);

    }catch(error){
        next(error);
    }
});

router.put('/approved_answer', (req, res) => {
    try {
        const user_id =  req.body.user_id;
        const questionId = req.body.question_id;

        answer.update({
            approved_answer: 1,
            },{ // Clause 
                where: {
                    id: answerId
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request answer does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.put('/flag_reason', auth, async(req, res) => {
    try {

        const flag_reason =  req.body.flag_reason;
        const id = req.body.id;

        answer.update({
                flag_reason: flag_reason
            },{ // Clause 
                where: {
                    id, id
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request question does not exist'); 
            }

            res.status(201).send(true);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.put('/:id', auth, async(req, res) => {
    try {

        const comment_answer =  req.body.comment_answer;
        const id = parseInt(req.params.id);

        answer.update({
                answer: comment_answer
            },{ // Clause 
                where: {
                    id, id
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request question does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.delete('/:id', [auth, admin],  async(req, res, next) => {
    try {
        const Id = parseInt(req.params.id);
        answer.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

function validateAnswer(data) {
    const schema = {
        user_id: Joi.number().required(),
        answer_id: Joi.number().required(),
        profile_id: Joi.number().required(),
        comment_answer: Joi.string().required()
    };

    return Joi.validate(data, schema); 
}

module.exports = router;