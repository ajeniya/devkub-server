const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');

const TechArticle = require('../models').article;
const User = require('../models').users;
const Profile = require('../models').profiles;
const ArticleComment = require('../models').article_comment;

const multer  = require('multer');
const cloudinary = require('cloudinary');
const cloudinaryStorage = require("multer-storage-cloudinary");
cloudinary.config({ 
    cloud_name: 'ajeniya', 
    api_key: '229196221183539', 
    api_secret: 'U1I7H6wkFzXJn_tqjqbBiT0eTmA' 
  });

const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


////////////////////////////////////////////////

const storage = cloudinaryStorage({
    cloudinary: cloudinary,
    folder: "articleImg",
    allowedFormats: ["jpg", "png", "jpeg", "gif"]
    //transformation: [{ width: 200, height: 300, crop: "limit" }]
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };

const upload = multer({
    storage: storage,
    limits: {
    fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});


router.post('/articleImg', upload.single('file'), async(req, res, next) => {
    try{
        const response = {
            "url": req.file.secure_url
        }
        res.status(200).send(response);

    } catch (error){
        next(error); 
    }
})

//////////////////////////////////////////////////////

router.get('/user', (req, res, next) => {
    try {
        const userArticleId = parseInt(req.body.user_id);

        TechArticle.findAndCountAll({
            include: [Profile, ArticleComment],
            where: {user_id: userArticleId },
            attributes: { exclude: ['password'] }
          }).then(ArticleResult => {
            
            if(_.isEmpty(ArticleResult)){
                return res.status(404).send('The question with the given ID was not found');
            }
            
            res.status(200).send( {
                count: ArticleResult.count,
                rows:  ArticleResult.rows
            });

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});


router.get('/', (req, res, next) => {
    try {
        TechArticle.findAndCountAll(
            {
                include: [Profile],
                attributes: { exclude: ['password'] }
            }
        ).then(ArticleResult => {

            res.status(200).send(
                {
                    count: ArticleResult.count,
                    rows:  ArticleResult.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/:id', (req, res, next) => {
    try {
        const articleId = parseInt(req.params.id);

        TechArticle.findOne({
            include: [Profile, ArticleComment],
            where: {id: articleId },
            attributes: { exclude: ['password'] }
          }).then(result => {
            
            if(_.isEmpty(result)){
                return res.status(404).send('The article with the given ARTICLE_ID was not found');
            }

            TechArticle.findByPk(articleId).then(article => {
                return article.increment('article_view_count', {by: 1})
            });
            
            res.status(200).send(result);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});


router.post('/wow', async(req, res, next) => {
    try{
        const articleId = parseInt(req.body.articleId);

        TechArticle.findByPk(articleId).then(article => {
            return article.increment('article_wow', {by: 1})
        }).catch((error) => {
            console.log(error)
        });

        res.status(200).send(true);

    }catch(error){
        next(error);
    }
});

router.post('/',[auth], async(req, res, next) => {
    try{
        console.log(req.body);
        TechArticle.create({ 
            user_id: req.user.user_id,
            article_heading: req.body.title,
            article_body: req.body.article,
            technology_tag: JSON.stringify(req.body.techTag),
            profile_id: req.user.profile_id,
            banner_image: req.body.banner_image,
         }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error);

         })

    } catch (error){
        next(error); 
    }
})


router.put('/:id', (req, res) => {
    try {
        const articleId = parseInt(req.params.id);

        const user_id =  req.body.user_id;
        const article_heading =  req.body.article_heading;
        const article_body = req.body.article_body;
        const technology_tag = req.body.technology_tag;
        const banner_image = req.body.banner_image;

        TechArticle.update({
                user_id: user_id,
                article_heading: article_heading,
                article_body: article_body,
                technology_tag: technology_tag,
                banner_image: banner_image
            },{ // Clause
                where: {
                    id: articleId,
                    user_id: user_id
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request article does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.delete('/:id',  (req, res, next) => {
    try {
        const Id = parseInt(req.params.id);
        TechArticle.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('Record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;