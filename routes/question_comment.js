const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');
const Joi = require('joi');

const user = require('../models').users;
const profile = require('../models').profiles;
const question_comment = require('../models').question_comment;

const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();
router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 

router.get('/comment', auth, async(req, res, next) => {
    try {
        const questionId = parseInt(req.body.question_id);

        question_comment.findAndCountAll({
            include: [user, profile],
            where: { question_id: questionId }
          }).then(questionsAnswerResult => {
            
            if(_.isEmpty(questionsAnswerResult)){
                return res.status(404).send('The question with the given ID was not found');
            }
            
            res.status(200).send( {
                count: questionsAnswerResult.count,
                rows:  questionsAnswerResult.rows
            });

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.get('/', auth, async(req, res, next) => {
    try {
        question_comment.findAndCountAll(
            {include: [user, profile]}
        ).then(QuestionAnswerResult => {
            res.status(200).send(
                {
                    count: QuestionAnswerResult.count,
                    rows:  QuestionAnswerResult.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/:id', auth, async(req, res, next) => {
    try {
        const Id = parseInt(req.params.id);

        question_comment.findAll({
            include: [user,profile],
            where: { question_id: Id }
          }).then(answerCommentResult => {
            
            if(_.isEmpty(answerCommentResult)){
                return res.status(404).send('The question comment with the given ID was not found');
            }
            
            res.status(200).send(answerCommentResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});


router.post('/', auth, async(req, res, next) => {
    try{

         const { question_id, comment_question} = req.body;

        question_comment.create({ 
            user_id: req.user.user_id,
            profile_id: req.user.profile_id,
            question_id: question_id,
            comment_question: comment_question
         }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    } catch (error){
        next(error); 
    }
});

router.post('/upvote', auth, async(req, res, next) => {
    try{

        const answerCommentId = parseInt(req.body.answerCommentId);

        question_comment.findByPk(answerCommentId).then(answerResult => {
            return answerResult.increment('upvote', {by: 1})
        }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    }catch(error){
        next(error);
    }
});

router.post('/downvote', auth, async(req, res, next) => {
    try{
        const answerCommentId = parseInt(req.body.answerCommentId);

        question_comment.findByPk(answerCommentId).then(article => {
            return article.increment('downvote', {by: 1})
        }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    }catch(error){
        next(error);
    }
});

router.put('/:id', auth, async(req, res) => {
    try {
        const Id = parseInt(req.params.id);

        const comment_question =  req.body.comment_question;

        question_comment.update({
            comment_question: comment_question,
            },{ // Clause 
                where: {
                    id: Id
                }
            }
        ).then(count => {
            console.log(count);
            if(count <= 0){
                res.status(404).send('The comment answer does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});


router.delete('/:id', [auth, admin],  async(req, res, next) => {
    try {
        const Id = parseInt(req.params.id);
        question_comment.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

function validateQuestionComment(data) {
    const schema = {
        comment_question: Joi.string().required()
    };

    return Joi.validate(data, schema); 
}

module.exports = router;