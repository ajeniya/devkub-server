const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');

const Job = require('../models').jobs;
const user = require('../models').users;
const job_apply_later = require('../models').job_apply_later;


const _ = require("lodash");
const bodyParser = require('body-parser');

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


router.get('/', (req, res, next) => {
    try {
        Job.findAndCountAll(
            { include: [user, job_apply_later] }
        ).then(JobResult => {
            res.status(200).send(
                {
                    count: JobResult.count,
                    rows:  JobResult.rows
                }
            );
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/:id', (req, res, next) => {
    try {
        const jobId = parseInt(req.params.id);

        Job.findAll({
            where: {id: jobId }
          }).then(jobResult => {
            
            if(_.isEmpty(jobResult)){
                return res.status(404).send('The job with the given JOB_ID was not found');
            }
            
            res.status(200).send(jobResult);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.post('/', async(req, res, next) => {
    try{
        Job.create({ 
            user_id: req.body.user_id,
            title: req.body.title,
            description: req.body.description,
            job_type: req.body.job_type,
            role: req.body.role,
            exprience_level: req.body.exprience_level,
            company_size: req.body.company_size,
            technology: req.body.technology,
            company_des: req.body.company_des,
            company_name: req.body.company_name,
            country: req.body.country,
            state: req.body.state
         }).then((created)=> {

            res.status(200).send(created);

         }).catch((error) => {

            console.log(error.Error);

         })

    } catch (error){
        next(error); 
    }
})


router.put('/:id', (req, res) => {
    try {
        const jobId = parseInt(req.params.id);

        const user_id =  req.body.user_id;
        const title =  req.body.title;
        const description = req.body.description;
        const job_type = req.body.job_type;
        const role = req.body.role;
        const exprience_level = req.body.exprience_level;
        const company_size = req.body.company_size;
        const technology = req.body.technology;
        const company_des = req.body.company_des;
        const company_name = req.body.company_name;

        Job.update({
                user_id: user_id,
                title: title,
                description: description,
                job_type: job_type,
                role: role,
                exprience_level: exprience_level,
                company_size: company_size,
                technology: technology,
                company_des: company_des,
                company_name: company_name
            },{ // Clause
                where: {
                    id: jobId,
                    user_id: user_id
                }
            }
        ).then(count => {
            if(count <= 0){
                res.status(404).send('The request job does not exist'); 
            }
            const updatedDataFromTheDb = {
                id: req.params.id
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            console.log(error);
            res.status(403).send(error);
        });

    } catch (error) {
        //next(error);
        console.log(error);
    }
});

router.delete('/:id', [auth, admin],  (req, res, next) => {
    try {
        const Id = parseInt(req.params.id);
        Job.destroy({
            where: {
                id: Id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

module.exports = router;