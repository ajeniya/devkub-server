const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');

const _ = require("lodash");
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const multer  = require('multer');
const cloudinary = require('cloudinary');
const cloudinaryStorage = require("multer-storage-cloudinary");
cloudinary.config({ 
    cloud_name: 'ajeniya', 
    api_key: '229196221183539', 
    api_secret: 'U1I7H6wkFzXJn_tqjqbBiT0eTmA' 
  });

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const Profile = require('../models').profiles;
const User = require('../models').users;

const router = express.Router();

router.use(bodyParser.json()); 
router.use(bodyParser.urlencoded({ extended: true })); 


////////////////////////////////////////////////

// const storage = multer.diskStorage({
//     destination: function(req, file, cb) {
//       cb(null, './public/images');
//     },
//     filename: function(req, file, cb) {
//       let extArray = file.mimetype.split("/");
//       let extension = extArray[extArray.length - 1];
//       cb(null, file.fieldname + '-' + Date.now()+ '.' +extension);
//     }
//   });

  const storage = cloudinaryStorage({
        cloudinary: cloudinary,
        folder: "avater",
        allowedFormats: ["jpg", "png"],
        transformation: [{ width: 200, height: 300, crop: "limit" }]
  });
  
  const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });

  //////////////////////////////////////////////////////

  
router.post('/avater', auth, upload.single('avater'), (req, res, next) => {
    try {
        const accountId = req.body.id;
        const avaterPath = req.file.secure_url;
        Profile.update(
            {
                avater:  avaterPath,
            },
            { // Clause
                where: 
                {
                    user_id: req.user.user_id
                }
            }
        ).then( (result)=> {
            res.status(200).send(avaterPath);
        }).catch( (error)=> {
            res.status(404).send('The request id does not exist'); 
        })
    } catch (error) {
        next(error);
    }
});



router.get('/me', auth, (req, res, next) => {
    try {
        Profile.findOne({
            where: {user_id: req.user.user_id },
            attributes: { exclude: ['password'] },
            include: [User]
          }).then(account => {
            res.status(200).send(account);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
          
    } catch (error) {
        next(error);
    }
    
});


router.get('/', (req, res, next) => {
    try {
        Profile.findAll({
            include: [User],
        }).then(userObject => {
            res.status(200).send(userObject);
        }).catch((error) => {
            res.status(400).send(error);
        });
    } catch (error) {
        next(error);
    }   
});


router.get('/:id', (req, res, next) => {
    try {
        const userId = parseInt(req.params.id);
        Profile.findOne({
            include: [User],
            where: {id: userId }
          }).then(account => {
            
            if(_.isEmpty(account)){
                return res.status(404).send('The user with the given ID was not found');
            }
            
            res.status(200).send(account);

          }).catch( (error)=> {
            res.status(400).send(error);
          })
        
    } catch (error) {
        next(error);
    }
   
});

router.post('/', auth, async(req, res, next) => {
    try {
        console.log(req.body);
        Profile.update(
        // Values to update
        {
            fullname: req.body.name, 
            title: req.body.title,
            about_me: req.body.aboutMe,
            country: req.body.country.id,
            technology_tag: JSON.stringify(req.body.technologies),
            state: req.body.state.id,
            city: req.body.city.id
        },
        { // Clause
            where: 
            {
                user_id: req.user.user_id
            }
        }).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('Unable to update profile, Try again'); 
            }
            
        res.status(200).send(true);

        }).catch((error)=> {
            res.status(403).send('Unable to update profile setting,');
        });
    } catch (error) {
        next(error);
    }
})

router.put('/:id', auth, (req, res, next) => {
    try {
        const Id = parseInt(req.params.id);

        const result = validateProfile(req.body);
        if (result.error) {
            res.status(400).send(result.error.details[0].message);
            return;
        }

        const user_id =  req.body.user_id;
        const full_name =  req.body.full_name;
        const title = req.body.title;
        const about_me = req.body.about_me;
        const technology_tag = req.body.technology_tag;
        const country = req.body.country.id;
        const state = req.body.state.id;
        const city = req.body.city.id;


        Account.update(
            // Values to update
            {
                full_name:  full_name,
                location: location,
                about_me: about_me,
                technology_tag: technology_tag,
                country: country,
                state: state,
                city: city
            },
            { // Clause
                where: 
                {
                    id: userId,
                    user_id: user_id
                }
            }
        ).then(count => {
            console.log('Rows updated ' + count);
            if(count <= 0){
                res.status(404).send('The request account does not exist'); 
            }
            const updatedDataFromTheDb = {
                user_id: userId,
                full_name:  full_name,
                title: title,
                about_me: about_me,
                technology_tag: technology_tag,
                country: country,
                state: state,
                city: city
            }
            res.status(201).send(updatedDataFromTheDb);
        }).catch((error)=> {
            res.status(403).send('Email taken by different user');
        });

    } catch (error) {
        next(error);
    }
});

router.delete('/:id', [auth, admin], (req, res, next) => {
    try {
        Account.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(function (deletedRecord) {
            if(deletedRecord === 1){
                res.status(200).send('Deleted successfully');          
            }
            else
            {
                res.status(404).send('record not found');
            }
        })
        .catch(function (error){
            res.status(500).send(error);
        });
        
      } catch (error) {
        next(error);
    }

});

function validateProfile(data) {
    const schema = {
        user_id: Joi.number().required(),
        full_name: Joi.string().required(),
        title: Joi.string().required(),
        about_me: Joi.string().required(),
        technology_tag: Joi.string().required(),
        country: Joi.string().required(),
        state: Joi.string().required(),
        city: Joi.string().required(),
    };

    return Joi.validate(data, schema); 
}


module.exports = router;